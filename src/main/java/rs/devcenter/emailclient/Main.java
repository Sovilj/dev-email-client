package rs.devcenter.emailclient;
import com.gilecode.reflection.ReflectionAccessUtils;
import rs.devcenter.emailclient.service.AuthenticationService;
import rs.devcenter.emailclient.service.DebugLogger;
import java.util.Scanner;

public class Main {

    protected static AuthenticationService authenticationService = null;
    DebugLogger mLogger;
    Scanner mScanner;

    public Main() {

        mLogger = DebugLogger.getInstance();
        mScanner = new Scanner(System.in, "UTF-8");
    }

    public static void main(String[] args) {

        Main publicClient = new Main();
        try {
            publicClient.startConnect();
            MainWindow.main(args);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            publicClient.mScanner.close();
        }
    }

    private void startConnect() throws Exception {

        ReflectionAccessUtils.suppressIllegalReflectiveAccessWarnings();
        authenticationService = AuthenticationService.getInstance();
        authenticationService.connect(mScanner);
    }

}

