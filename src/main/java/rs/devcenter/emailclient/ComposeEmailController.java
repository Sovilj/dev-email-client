package rs.devcenter.emailclient;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class ComposeEmailController {

    /**
     * This method is used to close the application
     * @param actionEvent
     */
    @FXML
    public void closeApplication(ActionEvent actionEvent){

        Platform.exit();
        System.exit(0);
    }
}
