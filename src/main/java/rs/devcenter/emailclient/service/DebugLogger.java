package rs.devcenter.emailclient.service;

import com.microsoft.graph.logger.LoggerLevel;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import static rs.devcenter.emailclient.service.AuthenticationService.DebugLevel;

public class DebugLogger {

    final Logger log;
    private static DebugLogger INSTANCE;

    {
        String loggerName = "com.microsoft.graphsample.connect";
        log = Logger.getLogger(loggerName);
        ConsoleHandler handler = new ConsoleHandler();
        handler.setLevel(Level.ALL);
        log.addHandler(handler);
        log.setLevel(Level.ALL);
    }

    public static synchronized DebugLogger getInstance() {

        if (INSTANCE == null) {
            INSTANCE = new DebugLogger();
        }
        return INSTANCE;
    }

    public void writeLog(Level logLevel, String message) {
        if (DebugLevel == LoggerLevel.DEBUG) {
            log.log(logLevel, message);
        }
    }
}