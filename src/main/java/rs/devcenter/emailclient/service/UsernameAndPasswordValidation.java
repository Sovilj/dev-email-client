package rs.devcenter.emailclient.service;

import java.util.regex.Pattern;

public class UsernameAndPasswordValidation {

    /**
     * Be between 6 and 20 characters long
     * Contain at least one lower case character.*
     */
    final String PASSWORD_PATTERN = "((?=.*[a-z]).{6,20})";

    /**
     * Must contains “@” symbol
     * Tld can not start with dot “.”
     * Last tld must contains at least two characters
     * Email’s first character can not start with dot “.”
     * Email’s is only allow character, digit, underscore and dash
     * Email’s tld is only allow character and digit
     * Double dots “.” are not allow
     * Email’s last character can not end with dot “.”
     * Double “@” is not allow
     * Email’s tld which has two characters can not contains digit
     */
    final String EMAIL_REGEX = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";

    /**
     * This method checks username(regex )
     *
     * @param username
     * @return true(username is valid) or false(username isn't valid)
     */
    public boolean isUsernameValid(String username) {

        Pattern pat = Pattern.compile(EMAIL_REGEX);
        if (username == null)
            return false;
        return pat.matcher(username).matches();
    }

    /**
     * This method checks password(regex)
     *
     * @param password
     * @return true(password is valid) or false(password isn't valid)
     */
    public boolean isPasswordValid(String password) {

        return Pattern.compile(PASSWORD_PATTERN).matcher(password).matches();

    }


}