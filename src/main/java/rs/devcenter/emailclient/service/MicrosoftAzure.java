package rs.devcenter.emailclient.service;

import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.oauth2.clientauthentication.ClientAuthentication;
import com.github.scribejava.core.oauth2.clientauthentication.RequestBodyAuthenticationScheme;

public class MicrosoftAzure extends DefaultApi20 {

    private static class InstanceHolder {
        static final MicrosoftAzure INSTANCE = new MicrosoftAzure();
    }

    @Override
    public String getAccessTokenEndpoint() {
        return "https://login.microsoftonline.com/common/oauth2/v2.0/token";
    }

    @Override
    protected String getAuthorizationBaseUrl() {
        return "https://login.microsoftonline.com/common/oauth2/v2.0/authorize";
    }

    public static DefaultApi20 instance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public ClientAuthentication getClientAuthentication() {
        return RequestBodyAuthenticationScheme.instance();
    }
}
