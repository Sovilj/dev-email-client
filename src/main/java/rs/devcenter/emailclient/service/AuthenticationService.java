package rs.devcenter.emailclient.service;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.microsoft.graph.logger.LoggerLevel;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.Scanner;

import static java.awt.Desktop.getDesktop;
import static java.awt.Desktop.isDesktopSupported;

public class AuthenticationService {

    public static String codeUser; //code after REDIRECT_URL for authentication
    public final static String CLIENT_ID = "a853395f-b08a-44cf-aabf-78364f9437a2"; //Application(Client) ID - Azure Active Directory
    public final static String REDIRECT_URL = "https://login.microsoftonline.com/common/oauth2/nativeclient";
    public final static String SCOPES = "Files.ReadWrite openid User.Read Mail.Send Mail.ReadWrite";
    public static final String PROTECTED_RESOURCE_URL = "https://graph.microsoft.com/v1.0/me";
    public static final LoggerLevel DebugLevel = LoggerLevel.ERROR;
    private static AuthenticationService INSTANCE;
    private OAuth20Service mOAuthService = null;
    private OAuth2AccessToken mAccessToken;

    {
        if (DebugLevel == LoggerLevel.DEBUG) {
            DebugLogger.getInstance().writeLog(Level.INFO, "AuthenticationManager initialization block called");

            try (OAuth20Service service = new ServiceBuilder(CLIENT_ID)
                    .callback(REDIRECT_URL)
                    .scope(SCOPES)
                    .apiKey(CLIENT_ID)
                    .debugStream(System.out)
                    .debug()
                    .build(MicrosoftAzure.instance())
            ) {
                mOAuthService = service;
            } catch (java.io.IOException | IllegalArgumentException ex) {
                try {
                    throw ex;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            try (OAuth20Service service = new ServiceBuilder(CLIENT_ID)
                    .callback(REDIRECT_URL)
                    .scope(SCOPES)
                    .apiKey(CLIENT_ID)
                    .build(MicrosoftAzure.instance())
            ) {
                mOAuthService = service;
            } catch (java.io.IOException | IllegalArgumentException ex) {
                try {
                    throw ex;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private AuthenticationService() {

        DebugLogger.getInstance().writeLog(Level.INFO, "AuthenticationManager constructor called");

    }

    public static synchronized AuthenticationService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AuthenticationService();
        }
        return INSTANCE;
    }

    /**
     * This method implements the Authorization Grant flow for OAuth 2.0
     * A user responds to the credentials challenge in the browser opened by this method. User
     * types their credentials in the browser window and is redirected to an authorization page.
     * User accepts the sample app's requests to access Microsoft Graph resources and an
     * authorization token is returned to the callback url.
     * In a breakpoint before the service.getAccessToken call, update the authorizationCode string variable
     * with the authorization code from the POST to the callback url.
     *
     * @throws URISyntaxException
     * @throws IOException
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public void connect(Scanner inputScanner) throws URISyntaxException, IOException, InterruptedException, ExecutionException {

        try {
            mAccessToken = mOAuthService.getAccessToken(getAuthorizationCode(inputScanner));
            System.out.println(mAccessToken.getRawResponse());
            makeAuthenticatedMeRequest();
        } finally {
        }
    }

    /**
     * Creates and runs REST authenticated request for Me resource synchronously. The response includes a
     * JSON payload with a representation of the me resource.
     *
     * @throws InterruptedException
     * @throws ExecutionException
     * @throws IOException
     */
    private void makeAuthenticatedMeRequest() throws InterruptedException, ExecutionException, IOException {

        final OAuthRequest request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL);
        mOAuthService.signRequest(mAccessToken, request);
        request.addHeader("Accept", "application/json, text/plain, */*");
        final Response response = mOAuthService.execute(request);
        System.out.println(response.getCode());
        System.out.println(response.getBody());
    }

    /**
     * Gets the user authorization grant flow URL, opens a browser tab with the resulting authorization token
     * embedded in the address URL. User copies the URL, extracts the token, and pastes it into the system console.
     * The Scanner reads the authorization code from the system console and returns the value to calling code.
     * <p>
     * The authorization code is returned when the user accepts the sample app's requests to access Microsoft Graph
     * resources. The authorization code lists the Microsoft Graph resources that the user has given the sample
     * app permission to access.
     *
     * @return Authorization code
     * @throws IOException
     * @throws URISyntaxException
     */
    private String getAuthorizationCode(Scanner inputScanner) throws IOException, URISyntaxException {

        final String authorizationUrl = mOAuthService.getAuthorizationUrl();
        if (isDesktopSupported()) {
            getDesktop().browse(new URI(authorizationUrl));
        } else {
            System.out.println(authorizationUrl);
        }
        String code = "";
        try {
            code = inputScanner.nextLine();
        } catch (Exception e) {
            e.printStackTrace();
        }
        codeUser = code;
        return code;
    }
}
