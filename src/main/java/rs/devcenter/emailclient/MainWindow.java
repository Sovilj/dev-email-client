package rs.devcenter.emailclient;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainWindow extends Application {

    /**
     * This method is used to open new window
     * @param stage
     * @throws Exception
     */
    public void start(Stage stage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("fxml/start.fxml"));
        Scene scene = new Scene(root, 400, 250);
        stage.setScene(scene);
        stage.setTitle("E M A I L   C L I E N T");
        stage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

}
