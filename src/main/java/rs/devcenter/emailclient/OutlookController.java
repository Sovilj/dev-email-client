package rs.devcenter.emailclient;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import rs.devcenter.emailclient.service.UsernameAndPasswordValidation;

import java.io.IOException;

import static javafx.fxml.FXMLLoader.load;

public class OutlookController {

    final UsernameAndPasswordValidation service = new UsernameAndPasswordValidation();

    @FXML
    private TextField usernameTextField;

    @FXML
    private TextField passwordTextField;

    /**
     * This method is used to open new window
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void confirmCodeButton(ActionEvent actionEvent) throws IOException {

        if ((service.isUsernameValid(usernameTextField.getText())) && (service.isPasswordValid(passwordTextField.getText()))) {

            Parent fxmlLoader = load(getClass().getResource("fxml/confirmCode.fxml"));
            Scene scene = new Scene(fxmlLoader,600,400);
            Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.show();

        } else {
            System.out.println("Email is incorrect, or password is incorrect!");
        }
    }
}
