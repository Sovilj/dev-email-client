package rs.devcenter.emailclient;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.io.IOException;
import static javafx.fxml.FXMLLoader.load;
import static rs.devcenter.emailclient.service.AuthenticationService.codeUser;

public class ConfirmCodeController {

    @FXML
    public TextField code;

    /**
     * This method is used to check whether the code is valid or not
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void dashboardButton(ActionEvent actionEvent) throws IOException {

        String codeValidation = code.getText();

        if (codeValidation.equals(codeUser)) {
            Parent fxmlLoader = load(getClass().getResource("fxml/dashboardOutlook.fxml"));
            Scene scene = new Scene(fxmlLoader, 600, 400);
            Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            window.setScene(scene);
            window.show();
        } else {
            System.out.println("Code is not valid");
        }
    }
}
