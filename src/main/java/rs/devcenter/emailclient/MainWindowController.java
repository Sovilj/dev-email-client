package rs.devcenter.emailclient;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainWindowController{

    /**
     * This method is used to open new window
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void outlookSignInButton(ActionEvent actionEvent) throws IOException {

        Parent fxmlLoader = FXMLLoader.load(getClass().getResource("fxml/loginOutlook.fxml"));
        Scene scene = new Scene(fxmlLoader,450, 300);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.setTitle("E M A I L   C L I E N T");
        window.show();
    }

    /**
     * This method is used to open new window
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void gmailSignInButton(ActionEvent actionEvent) throws IOException {

        Parent fxmlLoader = FXMLLoader.load(getClass().getResource("fxml/loginGmail.fxml"));
        Scene scene = new Scene(fxmlLoader,450, 300);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.setTitle("E M A I L   C L I E N T");
        window.show();
    }

}
