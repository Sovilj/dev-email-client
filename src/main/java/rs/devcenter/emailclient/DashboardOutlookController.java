package rs.devcenter.emailclient;

import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

import static javafx.fxml.FXMLLoader.load;

public class DashboardOutlookController {

    /**
     * This method is used to open new window
     * @param actionEvent
     * @throws IOException
     */
    public void composeEmailButton(ActionEvent actionEvent) throws IOException {

        Parent fxmlLoader = load(getClass().getResource("fxml/composeEmail.fxml"));
        Scene scene = new Scene(fxmlLoader,600,350);
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.setTitle("New message");
        window.show();
    }
}
