//package rs.devcenter.emailclient.repository.sqlite;
//
//import java.sql.*;
//
///**
// * This class is used to interact with the database
// *
// * @author Sovilj Ivana
// * @since
// */
//public class Connect {
//
//    /**
//     * This method is used to create database
//     *
//     * @return
//     */
//    public static Connection connect() {
//
//        String url = "jdbc:sqlite://home/ivana/Java/emailclientdev/database/database.db";
//
//        Connection connection = null;
//        try {
//            connection = DriverManager.getConnection(url);
//            System.out.println("Successful connection");
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//        }
//        return connection;
//    }
//
//    public static void createNewTable() {
//
//        String url = "jdbc:sqlite://home/ivana/Java/emailclientdev/database/database.db";
//
//        String sql = "CREATE TABLE IF NOT EXISTS xxxxx" +
//                "(xx dataType PRIMARY KEY ?AUTOINCREMENT?," +
//                "xx dataType NOT NULL/NULL, " +
//                "xx dataType NOT NULL/NULL," +
//                "xx dataType NOT NULL/NULL," +
//                "xx dataType NOT NULL/NULL," +
//                "xx dataType NOT NULL/NULL)";
//
//        try (Connection conn = DriverManager.getConnection(url);
//             Statement statement = conn.createStatement()) {
//            statement.execute(sql);
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//        }
//
//    }
//
//    public void insertIntoTable() {
//
//        String sql = "INSERT INTO xxxxx" +
//                "( xx, xx, xx, xx, xx) VALUES (?,?,?,?,?)";
//        try (Connection conn = this.connect();
//             PreparedStatement pStatement = conn.prepareStatement(sql)) {
//            // pStatement.setString(1, xx);
//            // pStatement.setString(2, xx);
//            // pStatement.setString(3, xx);
//            // pStatement.setString(4, xx);
//            // pStatement.setString(5, xx);
//            // pStatement.executeUpdate();
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//        }
//
//    }
//
//    public void selectAll() {
//
//        String sql = "SELECT xx,xx,xx,xx,xx, xx FROM xxxxx";
//
//        try (Connection connection = this.connect();
//             Statement stmt = connection.createStatement();
//             ResultSet rs = stmt.executeQuery(sql)) {
//
//            while (rs.next()) {
//
//                System.out.println(rs.getInt("xx") + rs.getString("xx") + rs.getString("xx") + rs.getString("xx") + rs.getString("xx") + rs.getString("xx"));
//            }
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//        }
//
//    }
//}